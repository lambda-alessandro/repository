package com.example.repository.controller;

import com.example.repository.entities.Persona;
import com.example.repository.repository.IPersonaRepository;
import com.example.repository.services.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
public class Controller
{
    private static Logger logger = LoggerFactory.getLogger(Controller.class);

    @Autowired
    Service tmp;

    @GetMapping("/CF")
    public String execute(@RequestParam String cf)
    {
        return tmp.getPersona(cf);
    }

    @PostMapping("/insert")
    public String executeInsert(@RequestBody String json)
    {
        return tmp.savePersona(json);
    }

   /* @PostMapping("/persona")
    public void execute(@RequestBody String json) throws IOException
    {
        Persona p2 = new Gson().fromJson(json, Persona.class);
        service.addElementAnagrafica(p2);
        //logger.info(p2.stampa());        //fase di deserializzazione
    }*/

}
