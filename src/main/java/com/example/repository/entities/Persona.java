package com.example.repository.entities;

import javax.persistence.*;

@Entity
@Table(name="Persona")
public class Persona
{
    @Id
    @Basic(optional = false)
    @Column(name = "CF")
    private String CF;

    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;

    @Basic(optional = false)
    @Column(name = "cognome")
    private String cognome;

    @Basic(optional = false)
    @Column(name = "eta")
    private int eta;

    @Basic(optional = false)
    @Column(name = "indirizzo")
    private String indirizzo;

    public String getCF() {
        return this.CF;
    }

    public void setCF(String CF) {
        this.CF = CF;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return this.cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getEta() {
        return this.eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    public String getIndirizzo() {
        return this.indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }
}
