package com.example.repository.repository;

import com.example.repository.entities.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPersonaRepository extends JpaRepository<Persona,String>
{
    public Persona findByCF(String CF);
    public Persona save(Persona persona);
}
