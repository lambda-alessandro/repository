package com.example.repository.services;

import com.example.repository.entities.Persona;
import com.example.repository.repository.IPersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.google.gson.Gson;
import org.springframework.web.bind.annotation.RequestParam;

@org.springframework.stereotype.Service
public class Service
{
    @Autowired
    IPersonaRepository repo;

    public String getPersona(String cf)
    {
        Persona tmp = repo.findByCF(cf);
        return tmp.getNome() + " " + tmp.getCognome() + " " + tmp.getCF() + " " + tmp.getIndirizzo() + " " + tmp.getEta();
    }

    public String savePersona(String json)
    {
        Persona p = new Gson().fromJson(json, Persona.class);
        return repo.save(p).toString();
    }
}
